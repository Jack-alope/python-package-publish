import setuptools

setuptools.setup(
    name="jacks-gitlab-trial-package",
    version="0.0.1",
    author="Jack F. Murphy",
    author_email="jack@mrph.dev",
    description="A small example package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
